# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?
## Nuevas tendencias en las tecnologías de la información
Dos expertos realizan una comparación sobre la evolución de la programación 
en las últimas dos décadas del pasado siglo y la actualidad.

Mapa:
```mermaid
graph TD
  A[Desarrollo en ordenadores] -->|en|B[Sistemas Antiguos]
  A-->|en|C[Sistemas Actuales]
  B-->|contaba con| D[- Pocos recursos de Hardware]
  D-->E[- Pocos lenguajes de programación] 
  B-->|era obligatorio| F[Comprender el Hardware]
  F-->|debido a| G[- Diferentes arquitecturas]
  G-->H[- Rendimiento del programa]
  H-->|depende|I[Solo del programador]
  I-->|lo que provoca una |J[Relación directa]


  C-->|cuenta con|L[- Hardware mucho más potente]
  L-->M[- Multiples lenguajes de programación]
  M-->N[Enfocados al programador]
  C-->O[Nuevas herramientas de desarrollo]
  O-->|como|P[Maquinas virtuales]
  P-->|que permiten|Q[Ejecutar lenguajes parcialmente compilados]
  Q-->|como|R[Java]
  R-->|por lo que| S[No es necesario conocer el Hardware]
  O-->|que usan| o[Multiples capas de abstracción]
  o-->|pudiendo generar|T[Dependencia a código de terceros]
  T-->|y|U[Sobrecarga de librerias y programas]
  U-->|provocando|V[Consumo mayor de recursos]
```
[Referencia](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)
---
# Hª de los algoritmos y de los lenguajes de programación
## Tendencias en las tecnologías de la información
De Euclides a Java. Historia de los algoritmos y de los lenguajes de programación", es el titulo de un libro escrito por Ricardo Peña Mari en el que se apoya la profesora Araujo para analizar la historia y alguno de los aspectos más importantes de la informática como los algoritmos y los lenguajes de programación.

Mapa: 
```mermaid
graph TD
A[Historia de los algoritmos y los lenguajes de programacion]-->B1[Algoritmo]
B1-->|es un|B12[Procedimiento sistematico y mecanico]
B12-->|usado para|B13[Resolver un problema]
B13-->|que|B15[Sea decidible]
B1-->|es|B14[Tan antiguo como la humanidad]
A-->E[Lenguaje de Programación]
E-->|es|E1[Notacion precisa y comoda]
E-->|destacan por su |E12[Paradigma]
E12-->|como|E13[Fortran-Imperativo-Alto Nivel]
E13-->E14[Lisp-Funcional]
E14-->E15[Simula-POO]
E15-->E16[Prolog-Lógica]
E-->|destacan por su |E17[Popularidad]
E17-->E18[Java,C++ y C]
E1-->|que|E11[Representa]
E11-->|un|B1
B1-->|son| C[Ejecutados en Maquinas]
C-->|que| C1[Evolucionan constantemente]
C1-->|empieza por|C11[ 1800 Telar de Jacquard]
C11-->C12[Primera maquina programable]
C11-->C13[Pianola]
C13-->C14[Máquina analítica de Charles Babbage]
C14-->C15[Arquitectura de Von Neumann]
C15-->|y|C16[Muchos otros avances]
```
[Referencia](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)
---

# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación
## Departamento de Lenguajes y Sistemas Informáticos
Mapa:
```plantuml
@startmindmap
+ Evolucion de los lenguajes y paradigmas de programación
++ Paradigma
+++_ es
++++ Forma de pensar o abordar el desarrollo de un problema
+++_ por ejemplo
++++ Estructurado
+++++ Separa de la dependencia de la arquitectura
+++++ Diseño de software modular
++++++_ presente en 
+++++++ Basic, C, Pascal, Fortran
++++ Funcional
+++++ Se basa en lenguaje matemático, utiliza funciones matemáticas.
++++++_ presente en 
+++++++ ML,Haskell y Miranda
++++ Lógico  
+++++ Basado en expresiones lógicas, predicados lógicos, utiliza valores booleanos
++++++_ presente en
+++++++ Prolog
++++ Concurrente
+++++ Solucion a problemas concurrentes, multiples usuarios accediendo al mismo tiempo a un mismo recurso.
++++ Distribuido
+++++ Surge para la elaboracion de programas localizados en mas de un equipo, permitiendo la comunicacion entre los modulos localizados en distintos ordenadores.
++++ Orientado a Objetos
+++++ Es una nueva forma de concebir el diseño de software, en la que las clases a programar son tratadas como abstracciones de objetos de la vida real.
++++++_ presente en 
+++++++ Java, C++
++++ Componentes
+++++ Expone una abstraccion aun mayor que la orientacion a objetos, permitiendo reutilizarlos para otros proyectos.
++++ Orientado a Aspectos
+++++ Programar los aspectos que debe cumplir el programa de manera separada, para reunir todo en conjunto una vez terminados de definirse los aspectos.
++ Lenguaje de Programación
+++_ surgen para
++++ Compensar el salto semantico
+++++_ entre
+++++ El lenguaje natural y el lenguaje maquina
@endmindmap
```
[Referencia](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc)